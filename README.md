# Custom Multi-level menu test assignment.

## Building

This project uses babel.js. Make sure you have latest gulp installed.

Install dependencies:

```
npm install
bower install
```

Build release:

```
gulp build
```

Your static minified deploy ready release will appear in dist folder.

## Developing

```
gulp serve
```
